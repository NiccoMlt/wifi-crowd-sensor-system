package it.unibo.crowd.model.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.github.pjgg.rxfirestore.Entity;
import it.unibo.crowd.model.AnonymousDevice;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import static it.unibo.crowd.model.entities.DeviceEntity.MISSING;

public class LocatedDeviceEntity implements Entity {
  public static final String COLLECTION = "located-devices";
  public static final String LOCATION = "location";
  public static final String DEVICES = "devices";
  @JsonProperty(value = LOCATION)
  private String location;
  @JsonProperty(value = DEVICES)
  private List<AnonymousDevice> devices;

  public LocatedDeviceEntity() {
    this(MISSING, new ArrayList<>());
  }

  public LocatedDeviceEntity(final String location, final List<AnonymousDevice> devices) {
    this.location = location;
    this.devices = devices;
  }

//  @Override
//  public HashMap<String, Object> toMap() {
//    final JsonObject json = new JsonObject();
//    final JsonArray devs = new JsonArray(getDevices());
//    json.put(DEVICES, devs);
//    json.put(LOCATION, getLocation());
//
//    final Map<String, Object> map = json.getMap();
//    return (map instanceof HashMap)
//      ? (HashMap<String, Object>) map
//      : new HashMap<>(map);
//  }

  @Override
  public String getCollectionName() {
    return COLLECTION;
  }

  @SuppressWarnings("unchecked")
  @Override
  public LocatedDeviceEntity fromJsonAsMap(final Map<String, Object> json) {
    setLocation((String) json.getOrDefault(LOCATION, MISSING));
    setDevices(
      ((List<Map<String, Object>>) json.getOrDefault(DEVICES, new ArrayList<Map<String, Object>>()))
        .stream()
        .map(map -> new AnonymousDeviceEntity().fromJsonAsMap(map))
        .collect(Collectors.toList())
    );
    return this;
  }

  @NotNull
  public List<AnonymousDevice> getDevices() {
    return devices;
  }

  public void setDevices(@NotNull final List<AnonymousDevice> devices) {
    this.devices = devices;
  }

  @NotNull
  public String getLocation() {
    return location;
  }

  public void setLocation(@NotNull final String location) {
    this.location = location;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) return true;
    if (!(o instanceof LocatedDeviceEntity)) return false;
    final LocatedDeviceEntity that = (LocatedDeviceEntity) o;
    return Objects.equals(getLocation(), that.getLocation()) &&
      Objects.equals(getDevices(), that.getDevices());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getLocation(), getDevices());
  }

//  @Override
//  public String toString() {
//    return new JsonObject(toMap()).toString();
//  }
}
