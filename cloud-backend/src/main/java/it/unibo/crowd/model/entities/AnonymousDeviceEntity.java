package it.unibo.crowd.model.entities;

import it.unibo.crowd.model.AnonymousDevice;

import java.util.Map;
import java.util.Objects;

/**
 * Data class that implements an Entity for the Google Cloud FireStore DB.
 * <p>
 * It models an anonymized version of [Device][it.unibo.crowd.model.Device] model class.
 */
public class AnonymousDeviceEntity extends DeviceEntity implements AnonymousDevice {

  public static final String COLLECTION = "devices";

  public AnonymousDeviceEntity() {
    super();
  }

  public AnonymousDeviceEntity(final String hashedMac, final String vendor) {
    super(hashedMac, vendor);
  }

  @Override
  public String getCollectionName() {
    return COLLECTION;
  }

  @Override
  public AnonymousDeviceEntity fromJsonAsMap(final Map<String, Object> json) {
    setHashedMac((String) json.getOrDefault(HASH_MAC, MISSING));
    setVendor((String) json.getOrDefault(VENDOR, MISSING));
    return this;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) return true;
    if (!(o instanceof DeviceEntity)) return false;
    DeviceEntity that = (DeviceEntity) o;
    return Objects.equals(getHashedMac(), that.getHashedMac()) &&
      Objects.equals(getVendor(), that.getVendor());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getHashedMac(), getVendor());
  }

//  @Override
//  public String toString() {
//    return new JsonObject(toMap()).toString();
//  }
}
