package it.unibo.crowd.model.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.github.pjgg.rxfirestore.Entity;
import org.jetbrains.annotations.NotNull;

import java.util.Map;
import java.util.Objects;

import static it.unibo.crowd.model.entities.DeviceEntity.MISSING;

/**
 * Data class that implements an Entity for the Google Cloud FireStore DB.
 * <p>
 * It models crowd measure in a given collection.
 */
public class CrowdEntity implements Entity {
  public static final String COLLECTION = "crowds";
  public static final String TIME = "time";
  public static final String ZONE = "zone";
  public static final String CROWD = "crowd";

  @JsonProperty(value = TIME)
  private String time;

  @JsonProperty(value = ZONE)
  private String zone;

  @JsonProperty(value = CROWD)
  private long crowd;

  public CrowdEntity() {
    this(MISSING, MISSING, 0);
  }

  public CrowdEntity(final String time, final String zone, final long crowd) {
    this.time = time;
    this.zone = zone;
    this.crowd = crowd;
  }

//  @Override
//  public HashMap<String, Object> toMap() {
//    final JsonObject json = new JsonObject();
//    json.put(CROWD, getCrowd());
//    json.put(LOCATION, getLocation());
//    json.put(TIME, getTime());
//
//    final Map<String, Object> map = json.getMap();
//    return (map instanceof HashMap)
//      ? (HashMap<String, Object>) map
//      : new HashMap<>(map);
//  }

  @Override
  public String getCollectionName() {
    return COLLECTION;
  }

  @Override
  public CrowdEntity fromJsonAsMap(final Map<String, Object> json) {
    setCrowd(((Number) json.getOrDefault(CROWD, 0)).longValue());
    setZone((String) json.getOrDefault(ZONE, MISSING));
    setTime((String) json.getOrDefault(TIME, MISSING));

    return this;
  }

  @NotNull
  public String getTime() {
    return time;
  }

  public void setTime(@NotNull final String time) {
    this.time = time;
  }

  @NotNull
  public String getZone() {
    return zone;
  }

  public void setZone(@NotNull final String zone) {
    this.zone = zone;
  }

  public long getCrowd() {
    return crowd;
  }

  public void setCrowd(final long crowd) {
    this.crowd = crowd;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) return true;
    if (!(o instanceof CrowdEntity)) return false;
    final CrowdEntity that = (CrowdEntity) o;
    return getCrowd() == that.getCrowd() &&
      Objects.equals(getTime(), that.getTime()) &&
      Objects.equals(getZone(), that.getZone());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getTime(), getZone(), getCrowd());
  }

//  @Override
//  public String toString() {
//    return new JsonObject(toMap()).toString();
//  }
}
