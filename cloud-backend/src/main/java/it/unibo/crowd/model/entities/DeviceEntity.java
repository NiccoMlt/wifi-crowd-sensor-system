package it.unibo.crowd.model.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.github.pjgg.rxfirestore.Entity;
import it.unibo.crowd.model.AnonymousDevice;
import org.jetbrains.annotations.NotNull;

import java.util.Map;
import java.util.Objects;

/**
 * Data class that implements an Entity for the Google Cloud FireStore DB.
 * <p>
 * It models an abstract anonymized version of [Device] model class.
 */
public abstract class DeviceEntity implements Entity, AnonymousDevice {
  /**
   * Default value for missing fields.
   */
  public static final String MISSING = "NONE";

  /**
   * Hashed MAC field name in FireStore DB.
   */
  public static final String HASH_MAC = "hash-mac";

  /**
   * Vendor field name in FireStore DB.
   */
  public static final String VENDOR = "vendor";

  @JsonProperty(value = HASH_MAC)
  private String hashedMac;

  @JsonProperty(value = VENDOR)
  private String vendor;

  public DeviceEntity() {
    this(MISSING, MISSING);
  }

  public DeviceEntity(final String hashedMac, final String vendor) {
    this.hashedMac = hashedMac;
    this.vendor = vendor;
  }

//  @Override
//  public HashMap<String, Object> toMap() {
//    final JsonObject json = new JsonObject();
//    json.put(HASH_MAC, getHashedMac());
//    json.put(VENDOR, getVendor());
//
//    final Map<String, Object> map = json.getMap();
//    return (map instanceof HashMap)
//      ? (HashMap<String, Object>) map
//      : new HashMap<>(map);
//  }

  @Override
  public abstract String getCollectionName();

  @Override
  public abstract DeviceEntity fromJsonAsMap(Map<String, Object> json);

  @NotNull
  @Override
  public String getHashedMac() {
    return hashedMac;
  }

  public void setHashedMac(@NotNull final String hashedMac) {
    this.hashedMac = hashedMac;
  }

  @NotNull
  @Override
  public String getVendor() {
    return vendor;
  }

  public void setVendor(@NotNull final String vendor) {
    this.vendor = vendor;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) return true;
    if (!(o instanceof DeviceEntity)) return false;
    DeviceEntity that = (DeviceEntity) o;
    return Objects.equals(getHashedMac(), that.getHashedMac()) &&
      Objects.equals(getVendor(), that.getVendor());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getHashedMac(), getVendor());
  }

//  @Override
//  public String toString() {
//    return new JsonObject(toMap()).toString();
//  }
}
