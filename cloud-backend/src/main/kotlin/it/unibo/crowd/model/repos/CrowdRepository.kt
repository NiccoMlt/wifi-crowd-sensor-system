package it.unibo.crowd.model.repos

import com.github.pjgg.rxfirestore.RxFirestoreSdk
import io.reactivex.Single
import io.vertx.reactivex.core.Vertx
import it.unibo.crowd.model.entities.CrowdEntity
import java.time.LocalDateTime
import java.util.function.Supplier

/**
 * Repository class that models a collection [Crowd measures][it.unibo.crowd.model.entities.CrowdEntity]
 * inside Google Cloud FireStore DB.
 *
 * @param vertx the current Vert.X context owner
 */
class CrowdRepository(vertx: Vertx = Vertx.currentContext().owner()) : RxFirestoreSdk<CrowdEntity>(
  Supplier { CrowdEntity() },
  vertx
) {

  /**
   * Get all crowd measures stored.
   *
   * @return single list of measures
   */
  fun getAll(): Single<List<CrowdEntity>> =
    this.queryBuilder(CrowdEntity.COLLECTION).flatMap { this.get(it) }

  /**
   * Get all crowd measures stored for a given location.
   *
   * @return single list of measures
   */
  fun getFor(location: String): Single<List<CrowdEntity>> =
    this.getAll().map { list -> list.filter { it.zone === location } }

  /**
   * Get the latest crowd measure stored for a given location.
   *
   * @return single measure
   */
  fun getLatestFor(location: String): Single<CrowdEntity> =
    this.getFor(location).map { list -> list.sortedBy { LocalDateTime.parse(it.time) }[0] }

  /**
   * Add the crowd measure using the timestamp and the location connected by dash (-) as a key.
   *
   * @param entity the measure to insert
   * @return single boolean
   */
  fun safeInsert(entity: CrowdEntity): Single<Boolean> =
    this.upsert("${entity.time}-${entity.zone}", CrowdEntity.COLLECTION, entity) // TODO: verify boolean usage
}
