package it.unibo.crowd.model.repos

import com.github.pjgg.rxfirestore.RxFirestoreSdk
import io.reactivex.Single
import io.vertx.reactivex.core.Vertx
import it.unibo.crowd.model.entities.AnonymousDeviceEntity
import java.util.function.Supplier

/**
 * Repository class that models a collection [AnonymousDevice][it.unibo.crowd.model.AnonymousDevice] inside
 * Google Cloud FireStore DB.
 *
 * @param vertx the current Vert.X context owner
 */
class DeviceRepository(vertx: Vertx = Vertx.currentContext().owner()) : RxFirestoreSdk<AnonymousDeviceEntity>(
  Supplier { AnonymousDeviceEntity() },
  vertx
) {

  /**
   * Add the Device using the hash of its MAC address as a key.
   *
   * @param entity the device to insert
   * @return single boolean
   */
  fun safeInsert(entity: AnonymousDeviceEntity): Single<Boolean> =
    this.upsert(entity.hashedMac, AnonymousDeviceEntity.COLLECTION, entity) // TODO: verify boolean usage

  /**
   * Get all devices stored.
   *
   * @return single list of devices
   */
  fun getAll(): Single<List<AnonymousDeviceEntity>> =
    this.queryBuilder(AnonymousDeviceEntity.COLLECTION).flatMap { this.get(it) }
}
