package it.unibo.crowd.model.repos

import com.github.pjgg.rxfirestore.RxFirestoreSdk
import io.reactivex.Single
import io.vertx.reactivex.core.Vertx
import it.unibo.crowd.model.AnonymousDevice
import it.unibo.crowd.model.entities.LocatedDeviceEntity
import java.util.function.Supplier

class LocatedDeviceRepository(vertx: Vertx = Vertx.currentContext().owner()) : RxFirestoreSdk<LocatedDeviceEntity>(
  Supplier { LocatedDeviceEntity() },
  vertx
) {

  fun insertDeviceAtLocation(location: String, device: AnonymousDevice): Single<Boolean> {
    return this.queryBuilder(LocatedDeviceEntity.COLLECTION)
      .map { it.whereEqualTo(LocatedDeviceEntity.LOCATION, location) }
      .flatMap { this.get(it) }
      .map { list ->
        if (list.size > 1) {
          throw Error("Multiple matching locations found")
        } else {
          if (list.size < 1) {
            return@map LocatedDeviceEntity(location, listOf(device))
          } else {
            val entity = list[0]
            if (entity.devices.none { it.hashedMac == device.hashedMac }) {
              entity.devices.plusAssign(device)
            }
            return@map entity
          }
        }
      }
      .flatMap { this.upsert(it.location, LocatedDeviceEntity.COLLECTION, it) }
  }
}
