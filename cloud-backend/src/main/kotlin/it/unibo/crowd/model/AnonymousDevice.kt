package it.unibo.crowd.model

/** The interface models an anonymized version of [Device]. */
interface AnonymousDevice {
  /** the MAC address anonymized with an hash function*/
  val hashedMac: String

  /** the vendor obtained from MAC address before anonymization */
  val vendor: String
}
