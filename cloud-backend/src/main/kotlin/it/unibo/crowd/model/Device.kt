package it.unibo.crowd.model

import com.samclarke.android.util.HashUtils
import inet.ipaddr.MACAddressString
import inet.ipaddr.mac.MACAddress
import io.vertx.core.json.JsonObject
import it.unibo.crowd.model.entities.AnonymousDeviceEntity
import java.net.URL

/**
 * Class that a Device with a MAC address.
 *
 * @param mac the MAC address as string
 * @constructor builds the object from a MAC address as string
 * @throws IllegalArgumentException if not valid ([MACAddressString.isValid])
 */
class Device(mac: String) : AnonymousDevice {
  private val macAddress: MACAddress

  /** Get the MAC address as colon-delimited string. */
  val mac: String
    get() = macAddress.toColonDelimitedString()

  init {
    val macAddr = MACAddressString(mac)
    if (macAddr.isValid) {
      this.macAddress = macAddr.toAddress()
    } else {
      throw IllegalArgumentException("MAC address is not valid")
    }
  }

  /**
   * Anonymize this device.
   *
   * @param hash a synchronous function to hash the MAC address
   * @param getVendor a synchronous function to get the vendor from MAC address
   */
  fun anonymize(
    hash: (mac: String) -> String = DEFAULT_HASH_FUN,
    getVendor: (mac: String) -> String = DEFAULT_VENDOR_FUN
  ): AnonymousDevice = AnonymousDeviceEntity(
    hash(mac),
    getVendor(mac)
  )

  /**
   * Anonymize this device asynchronously.
   *
   * Useful when the hash function to use is very slow, or connection to vendors DB is slow.
   *
   * @param hash a suspend function to hash the MAC address
   * @param getVendor a suspend function to get the vendor from MAC address
   */
  suspend fun anonymizeAsync(
    hash: suspend (mac: String) -> String,
    getVendor: suspend (mac: String) -> String
  ): AnonymousDevice = AnonymousDeviceEntity(
    hash(mac),
    getVendor(mac)
  )

  override val hashedMac: String
    get() = DEFAULT_HASH_FUN(mac)

  override val vendor: String
    get() = DEFAULT_VENDOR_FUN(mac)

  override fun equals(other: Any?): Boolean {
    if (this === other) return true
    if (javaClass != other?.javaClass) return false

    other as Device

    if (macAddress != other.macAddress) return false

    return true
  }

  override fun hashCode(): Int {
    return macAddress.hashCode()
  }

  companion object {
    private val DEFAULT_HASH_FUN: (mac: String) -> String = HashUtils::sha512

    private val DEFAULT_VENDOR_FUN: (mac: String) -> String = {
      JsonObject(URL("https://macvendors.co/api/$it").readText())
        .getJsonObject("result")
        .getString("company")
    }
  }
}
