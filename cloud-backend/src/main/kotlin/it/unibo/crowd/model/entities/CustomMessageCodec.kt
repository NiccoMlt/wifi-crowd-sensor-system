package it.unibo.crowd.model.entities

import io.vertx.core.buffer.Buffer
import io.vertx.core.eventbus.MessageCodec
import io.vertx.core.json.JsonObject
import java.util.HashMap

@Deprecated(message = "Never really used")
class CustomMessageCodec : MessageCodec<HashMap<String, Any>, HashMap<String, Any>> {

  // Easiest ways is using JSON object
  override fun encodeToWire(buffer: Buffer, customMessage: HashMap<String, Any>) {
    val jsonToEncode = JsonObject()
    customMessage.entries.forEach { jsonToEncode.put(it.key, it.value) }
    // Encode object to string
    val jsonToStr: String = jsonToEncode.encode()
    // Length of JSON: is NOT characters count
    val length = jsonToStr.toByteArray().size
    // Write data into given buffer
    buffer.appendInt(length)
    buffer.appendString(jsonToStr)
  }

  override fun decodeFromWire(position: Int, buffer: Buffer): HashMap<String, Any> {
    var pos = position
    // Length of JSON
    val length: Int = buffer.getInt(pos)
    // Get JSON string by it`s length
    // Jump 4 because getInt() == 4 bytes
    val jsonStr: String = buffer.getString(4.let { pos += it; pos }, length.let { pos += it; pos })
    val contentJson = JsonObject(jsonStr)
    // Get fields
    return HashMap(contentJson.map)
  }

  // If a message is sent *locally* across the event bus.
  override fun transform(customMessage: HashMap<String, Any>): HashMap<String, Any> {
    return customMessage
  }

  // Each codec must have a unique name.
  override fun name(): String {
    // This is used to identify a codec when sending a message and for unregistering codecs.
    return this.javaClass.simpleName
  }

  // Always -1
  override fun systemCodecID(): Byte {
    return -1
  }
}
