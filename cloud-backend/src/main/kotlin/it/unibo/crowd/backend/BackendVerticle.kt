package it.unibo.crowd.backend

import io.vertx.ext.auth.jwt.JWTAuth
import io.vertx.ext.web.Router
import io.vertx.ext.web.handler.JWTAuthHandler
import io.vertx.ext.web.handler.LoggerFormat
import io.vertx.ext.web.handler.LoggerHandler
import io.vertx.ext.web.handler.StaticHandler
import io.vertx.kotlin.core.http.listenAwait
import io.vertx.kotlin.coroutines.CoroutineVerticle
import io.vertx.kotlin.ext.auth.jwt.jwtAuthOptionsOf
import io.vertx.kotlin.ext.auth.pubSecKeyOptionsOf
import io.vertx.kotlin.ext.web.api.contract.openapi3.OpenAPI3RouterFactory
import it.unibo.crowd.backend.routes.addDevice
import it.unibo.crowd.backend.routes.addMeasure
import it.unibo.crowd.backend.routes.authenticate
import it.unibo.crowd.backend.routes.getCrowd
import it.unibo.crowd.backend.routes.getCrowdFor
import it.unibo.crowd.backend.routes.getDevices
import it.unibo.crowd.backend.routes.getLatestCrowdFor
import it.unibo.crowd.backend.routes.getVendor
import it.unibo.crowd.model.repos.CrowdRepository
import it.unibo.crowd.model.repos.DeviceRepository
import it.unibo.crowd.model.repos.LocatedDeviceRepository
import mu.KLogger
import mu.KotlinLogging

/**
 * This verticle serves the Angular application and implement OpenAPI contract for REST APIs.
 *
 * @param port the port the server would listen to
 */
class BackendVerticle(private val port: Int = DEFAULT_PORT) : CoroutineVerticle() {
  private val logger: KLogger = KotlinLogging.logger {}

  companion object {
    private const val DEFAULT_PORT: Int = 8080
  }

  override suspend fun start() {
    val deviceRepository = DeviceRepository()
    val locatedDeviceRepository = LocatedDeviceRepository()
    val crowdRepository = CrowdRepository()
    val router: Router = Router.router(vertx)
    router.route().handler(LoggerHandler.create(LoggerFormat.SHORT))

    val provider = JWTAuth.create(
      vertx, jwtAuthOptionsOf(
        pubSecKeys = listOf(
          pubSecKeyOptionsOf(
            algorithm = "RS256",
            publicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtAspEo0o3MXiCE4Ru56K\n" +
              "2Py3LRdUf8eZxJ03PZbscuWG2WimB7CcvrwDY2wHzApmkquhplGbtGwzeNflLkI3\n" +
              "JUnxGj4UV8PCT1Xh/+brtwdGF6OFeu5AFXoVZcVwgvdILzEcnvN2SBEdH/wGNVW0\n" +
              "ZJUchT6Jc+jDFNcVQ1UV0hsOG/97k7iLJi3dgP6bhNFfg21lCamcWQHSL3Yvtgo+\n" +
              "tvO7LgWXhg/hHnDck7mw7zAICd2kIcvQnyAz5bmG5d5F0PPSDr5/IZ/sVVKXYHrb\n" +
              "bp6MIubmF8HJwSbDxGfywrXG0QOmNATsfBGpL8lXfctnxD0qHB2XsdqSK7Iea2mM\n" +
              "iwIDAQAB",
            secretKey = "MIIEwAIBADANBgkqhkiG9w0BAQEFAASCBKowggSmAgEAAoIBAQC0CykSjSjcxeII\n" +
              "ThG7norY/LctF1R/x5nEnTc9luxy5YbZaKYHsJy+vANjbAfMCmaSq6GmUZu0bDN4\n" +
              "1+UuQjclSfEaPhRXw8JPVeH/5uu3B0YXo4V67kAVehVlxXCC90gvMRye83ZIER0f\n" +
              "/AY1VbRklRyFPolz6MMU1xVDVRXSGw4b/3uTuIsmLd2A/puE0V+DbWUJqZxZAdIv\n" +
              "di+2Cj6287suBZeGD+EecNyTubDvMAgJ3aQhy9CfIDPluYbl3kXQ89IOvn8hn+xV\n" +
              "Updgettunowi5uYXwcnBJsPEZ/LCtcbRA6Y0BOx8EakvyVd9y2fEPSocHZex2pIr\n" +
              "sh5raYyLAgMBAAECggEBAKN+kwCmsRmylKzIADBpK1saOlk3G2Oh5Q/3WGeUewln\n" +
              "R0p8TnEjP2OxJj6NppLySu0b8IIxD30K2L7/Br44IQ26lLgo8Z41RWl36u/QSfL+\n" +
              "JvdmZdsZKZaXM4EkhyRW2/UG2x+4pIuTBui5mwWp21OPWMPi4z0ffK+bFVL8nAEb\n" +
              "TDnkfdZ1/QZ3FpoTOTag3P4Rs5D82m6Z5I3AcOKR1nlfMpJEDXbJTmzM9thrb6CY\n" +
              "zVqVZRcqZuC7F92ZgqBxhdigtU9k+0G4/lR2PK3lR7F7Zwdf3Rn2161qwa6Xs707\n" +
              "qcajWfNDG53cOMOmbxt84hrGRoLQa61PDRDkwNiIdvECgYEA36XD9SchZoCYETAd\n" +
              "fT0/Tg2gaNaOFyqxhqGkIuf5IhDwGjA/htzqX/kfTgAiYqfbTWbDJp73mCUZxUro\n" +
              "q6JYTsRfGUAZZFaDWygL/AEwc+lwG3dQIsQwzAOt3FAdd2AJuAoYsmbm9ElF49GX\n" +
              "xqHsWiLdDJGAcqWyW/Xs/4tp4rkCgYEAzhahj0xtoGJXo9VC4z7MoJQPMlhQesB+\n" +
              "Le9Q3LjJwdC+QeyjR4PjHx7yPniIavUG57HMe4V0g7lBEqw0nQ3zZWE3aqSKMPBw\n" +
              "i/CbsQwdjbXwfahfLiKrqIDQVm79X+ym9dbm4521hxRy8wEEJpsZHz4VsC6WC4TO\n" +
              "AG6RoKO6V2MCgYEArX5SfaNVbcbkMLE6ryUMLuizP6DxqVcdUGukgM+jnKG3IRm8\n" +
              "rkpCJyGC/2drRU0h+vF1Akjfmc9uJnbt9/eAudnsTBJFoGjcjaKLaXsis0isByKP\n" +
              "r+iyUhGolfgwY6LE51gtwHfDQk51QT4el6CVUXcqM83L7yRLMLuelr737UECgYEA\n" +
              "hp1RuoB5uyGBjYpnvnfuwZT0uDXnm8bQpMCFWSd64havr/AaV0RWvqVAJn4k3Xhf\n" +
              "okgJAUYS7Ve8oa3KUCwkqDvwLsB7Y6wqV746dFMtmrsU839fbItJBS/e5hZpepbi\n" +
              "GALfnh4NzMYWr5QiWfVFW5r6H7LOX4fH55BRn1RTC3ECgYEA3XZrH6rmSTiI5ANg\n" +
              "ykV9SXbbjXvBh0uTOVyHbuHXsqkf2nkZoX1nCWkhjZiJatpbxETyXyls1YIamBfi\n" +
              "YFq+I3AENS8IJmVlqW68Rz5CAB+EmE/3TQySLnVQ8ZEVJTLOlPDYk5PsdNOwAC2f\n" +
              "JF5p1onDFKXo10UY1w1MAph/ykI="
          )
        )
      )
    )

    val apiRouter: Router = OpenAPI3RouterFactory
      .createAwait(vertx, "/openapi.yaml")
      .addHandlerByOperationId("getDevices", getDevices(deviceRepository))
      .addHandlerByOperationId("addDevice", addDevice(deviceRepository, locatedDeviceRepository))
      .addHandlerByOperationId("getCrowd", getCrowd(crowdRepository))
      .addHandlerByOperationId("getCrowdFor", getCrowdFor(crowdRepository))
      .addHandlerByOperationId("getLatestCrowdFor", getLatestCrowdFor(crowdRepository))
      .addHandlerByOperationId("addMeasure", addMeasure(crowdRepository))
      .addHandlerByOperationId("getVendor", getVendor(deviceRepository))
      .addHandlerByOperationId("authenticate", authenticate(provider, "pippo"))
      .addSecurityHandler("bearerAuth", JWTAuthHandler.create(provider))
      .router

    router.mountSubRouter("/api/", apiRouter)
    router.get().handler(StaticHandler.create())

    vertx
      .createHttpServer()
      .requestHandler(router)
      .listenAwait(port)

    logger.info { "HTTP server started on port $port" }
  }
}
