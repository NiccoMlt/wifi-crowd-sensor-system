@file:JvmName("CrowdRoutes")

package it.unibo.crowd.backend.routes

import io.vertx.core.Handler
import io.vertx.core.json.Json
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.RoutingContext
import it.unibo.crowd.model.entities.CrowdEntity
import it.unibo.crowd.model.repos.CrowdRepository
import org.apache.http.HttpStatus

/**
 * Generate a route handler for getCrowd operationId.
 *
 * @param crowdRepository the entry point to the crowd measure collection in DB.
 * @return the handler (a function) for Vert.X
 */
fun getCrowd(crowdRepository: CrowdRepository): Handler<RoutingContext> = Handler { routingContext: RoutingContext ->
  crowdRepository
    .getAll()
    .subscribe { measures ->
      routingContext.response().end(Json.encode(measures))
    }
}

/**
 * Generate a route handler for getCrowdFor operationId.
 *
 * @param crowdRepository the entry point to the crowd measure collection in DB.
 * @return the handler (a function) for Vert.X
 */
fun getCrowdFor(crowdRepository: CrowdRepository): Handler<RoutingContext> = Handler { routingContext: RoutingContext ->
  val location: String = routingContext.pathParam("location")
  crowdRepository
    .getFor(location)
    .subscribe { measures ->
      routingContext.response().end(Json.encode(measures))
    }
}

/**
 * Generate a route handler for getLatestCrowdFor operationId.
 *
 * @param crowdRepository the entry point to the crowd measure collection in DB.
 * @return the handler (a function) for Vert.X
 */
fun getLatestCrowdFor(crowdRepository: CrowdRepository): Handler<RoutingContext> {
  return Handler { routingContext: RoutingContext ->
    val location: String = routingContext.pathParam("location")
    crowdRepository
      .getLatestFor(location)
      .subscribe { latest ->
        routingContext.response().end(Json.encode(latest))
      }
  }
}

/**
 * Generate a route handler for addMeasure operationId.
 *
 * @param crowdRepository the entry point to the crowd measure collection in DB.
 * @return the handler (a function) for Vert.X
 */
fun addMeasure(crowdRepository: CrowdRepository): Handler<RoutingContext> {
  return Handler { routingContext: RoutingContext ->
    val json: JsonObject = routingContext.bodyAsJson
    val location: String = routingContext.pathParam("location")
    val measure = CrowdEntity(
      json.getString(CrowdEntity.TIME),
      json.getString(CrowdEntity.ZONE),
      json.getLong(CrowdEntity.CROWD)
    )
    if (measure.zone != location) throw IllegalArgumentException("Invalid location set")

    crowdRepository.safeInsert(measure).subscribe { _ ->
      routingContext.response().setStatusCode(HttpStatus.SC_NO_CONTENT).end()
    }
  }
}
