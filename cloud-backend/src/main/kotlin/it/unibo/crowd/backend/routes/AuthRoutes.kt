@file:JvmName("AuthRoutes")

package it.unibo.crowd.backend.routes

import io.vertx.core.Handler
import io.vertx.ext.auth.jwt.JWTAuth
import io.vertx.ext.web.RoutingContext
import io.vertx.kotlin.core.json.get
import io.vertx.kotlin.core.json.json
import io.vertx.kotlin.core.json.jsonObjectOf
import io.vertx.kotlin.core.json.obj
import io.vertx.kotlin.ext.auth.jwt.jwtOptionsOf

fun authenticate(provider: JWTAuth, secret: String): Handler<RoutingContext> =
  Handler { routingContext: RoutingContext ->
    val body = routingContext.bodyAsJson
    val key: String = body["key"]
    if (key == secret) {
      val location: String = body["location"]
      val token = provider.generateToken(
        json {
          obj("sub" to location)
        }, jwtOptionsOf(algorithm = "RS256")
      )
      routingContext.response().setStatusCode(200).end(jsonObjectOf("token" to token).toString())
    } else {
      routingContext.response().setStatusCode(401).end()
    }
  }
