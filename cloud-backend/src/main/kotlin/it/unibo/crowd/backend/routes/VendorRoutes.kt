@file:JvmName("VendorRoutes")

package it.unibo.crowd.backend.routes

import io.vertx.core.Handler
import io.vertx.core.json.Json
import io.vertx.ext.web.RoutingContext
import it.unibo.crowd.model.repos.DeviceRepository

/**
 * Generate a route handler for getVendor operationId.
 *
 * @param deviceRepository the entry point to the device collection in DB.
 * @return the handler (a function) for Vert.X
 */
fun getVendor(deviceRepository: DeviceRepository): Handler<RoutingContext> = Handler { routingContext: RoutingContext ->
  deviceRepository
    .getAll()
    .map { list -> list.map { it.vendor }.distinct() }
    .subscribe { vendors ->
      routingContext.response().end(Json.encode(vendors))
    }
}
