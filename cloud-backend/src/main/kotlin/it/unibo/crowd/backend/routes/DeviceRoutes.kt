@file:JvmName("DeviceRoutes")

package it.unibo.crowd.backend.routes

import io.vertx.core.Handler
import io.vertx.core.json.Json
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.RoutingContext
import it.unibo.crowd.model.entities.AnonymousDeviceEntity
import it.unibo.crowd.model.entities.DeviceEntity
import it.unibo.crowd.model.repos.DeviceRepository
import it.unibo.crowd.model.repos.LocatedDeviceRepository
import org.apache.http.HttpStatus.SC_INTERNAL_SERVER_ERROR
import org.apache.http.HttpStatus.SC_NO_CONTENT

/**
 * Generate a route handler for getDevice operationId.
 *
 * @param deviceRepository the entry point to the device collection in DB.
 * @return the handler (a function) for Vert.X
 */
fun getDevices(deviceRepository: DeviceRepository): Handler<RoutingContext> =
  Handler { routingContext: RoutingContext ->
    deviceRepository
      .getAll()
      .subscribe { devices ->
        routingContext.response().end(Json.encode(devices))
      }
  }

/**
 * Generate a route handler for addDevice operationId.
 *
 * @param deviceRepository the entry point to the device collection in DB.
 * @return the handler (a function) for Vert.X
 */
fun addDevice(
  deviceRepository: DeviceRepository,
  locatedDeviceRepository: LocatedDeviceRepository
): Handler<RoutingContext> = Handler { routingContext ->
  val json: JsonObject = routingContext.bodyAsJson
  val location: String = routingContext.pathParam("location")
  try {
    val device = AnonymousDeviceEntity(
      json.getString(DeviceEntity.HASH_MAC),
      json.getString(DeviceEntity.VENDOR)
    )
    deviceRepository.safeInsert(device).subscribe { success, exception ->
      if (success) {
        locatedDeviceRepository.insertDeviceAtLocation(location, device).subscribe { secondSuccess, secondException ->
          if (secondSuccess) {
            routingContext.response().setStatusCode(SC_NO_CONTENT).end()
          } else {
            val resp2 = "Error in 2nd: ${secondException.message}"
            println(resp2)
            routingContext
              .response()
              .setStatusCode(SC_INTERNAL_SERVER_ERROR)
              .setStatusMessage(resp2)
              .end(resp2)
          }
        }
      } else {
        val resp1 = "Error in 1st: ${exception.message}"
        println(resp1)
        routingContext
          .response()
          .setStatusCode(SC_INTERNAL_SERVER_ERROR)
          .setStatusMessage(resp1)
          .end(resp1)
      }
    }
  } catch (e: Exception) {
    val resp0 = "Error somewhere: ${e.message}"
    println(resp0)
    routingContext
      .response()
      .setStatusCode(SC_INTERNAL_SERVER_ERROR)
      .setStatusMessage(resp0)
      .end(resp0)
  }
}
