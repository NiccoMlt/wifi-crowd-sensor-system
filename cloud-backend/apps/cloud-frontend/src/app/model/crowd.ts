export interface Crowd {
  time: string
  zone: string
  crowd: number
}
