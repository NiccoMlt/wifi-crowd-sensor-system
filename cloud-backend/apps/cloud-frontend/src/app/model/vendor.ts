export interface Vendor {
  vendor: string
  quantity: number
}
