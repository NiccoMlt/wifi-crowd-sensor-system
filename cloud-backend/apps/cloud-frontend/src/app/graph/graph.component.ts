import {Component, OnInit, ViewChild} from '@angular/core';
import { CrowdService } from '../services/crowd.service';
import { Observable } from 'rxjs';
import { Crowd } from '../model/crowd';
import { map } from 'rxjs/operators';
import { ChartDataSets, ChartOptions, ChartPoint, ChartType } from 'chart.js';
import {BaseChartDirective} from "ng2-charts";
import 'chartjs-plugin-streaming';

@Component({
  selector: 'cloud-frontend-graph',
  templateUrl: './graph.component.html',
  styleUrls: ['./graph.component.scss']
})
export class GraphComponent implements OnInit {
  public lineChartLegend = true;
  public lineChartType: ChartType = 'line';
  public dataSets: ChartDataSets[] = [];

  public lineChartOptions: ChartOptions | unknown = {
    responsive: true,
    legend: {
      display: true
    },
    scales: {
      xAxes: [{
        type: 'time',
        realtime: {
          duration: 7500000, //TODO scale properly
          refresh: 6000,
          delay: 1000,
          onRefresh: chart => {
            this.mapDataFromAPI();
            this.crowdDataMapped.subscribe(data => {
              this.addDataToDataSets(data);
              chart.update({
                preservation: false
              });
            })
          }
        },
        distribution: 'series',
        scaleLabel: {
          display: true,
          labelString: "Time",
          fontColor: "red"
        }
      }],
      yAxes: [{
        scaleLabel: {
          display: true,
          labelString: "Crowd detected",
          fontColor: "green"
        }
      }]
    }
  };

  @ViewChild(BaseChartDirective, { static: true }) chart: BaseChartDirective;

  crowdData: Observable<Crowd[]>;
  crowdDataMapped: Observable<Map<string, Omit<Crowd, 'zone'>[]>>;

  constructor(private _crowdService: CrowdService) {}


  private static sortArrayByDate(arrayToSort: Pick<Crowd, "time" | "crowd">[]): void {
    arrayToSort.sort((a: Pick<Crowd, "time" | "crowd">, b: Pick<Crowd, "time" | "crowd">) => {
      return new Date(a.time).getTime() - new Date(b.time).getTime();
    });
  }

  private static cutOldData(pointDate: string): boolean {
    let now = new Date();
    now.setHours(now.getHours() - 2);
    return new Date(now).getTime() < new Date(pointDate).getTime();
  }

  private addDataSets() {
    this.crowdDataMapped.subscribe(data => {
      const labels: ChartDataSets[] = [];
      for (const key of data.keys()) {
        labels.push({
          label: key,
          data: []
        });
      }
      this.dataSets.push(...labels);
      this.addDataToDataSets(data);
    });
  }

  private addDataToDataSets(data: Map<string, Pick<Crowd, "time" | "crowd">[]>) {
    this.dataSets.forEach((dataset: ChartDataSets) => {
      data.get(dataset.label).forEach(single => {
        const point: ChartPoint = {
          x: single.time,
          y: single.crowd
        };
        if(!dataset.data.some((x: number & ChartPoint) => x.x === point.x && x.y === point.y)){
          GraphComponent.sortArrayByDate(data.get(dataset.label));
          if(GraphComponent.cutOldData(point.x as string)) {
            dataset.data.push(point as number & ChartPoint);
            console.log("updated " + point);
          }
        }
      });
    });
  }

  private mapDataFromAPI() {
    this.crowdDataMapped = this.crowdData.pipe(
      map(data =>
        data.reduce((crowdMap: Map<string, Omit<Crowd, 'zone'>[]>, d: Crowd) => {
          const { zone, time, crowd } = d;

          if (crowdMap.has(zone)) {
            crowdMap.get(zone).push({ crowd, time });
          } else {
            crowdMap.set(zone, [{ crowd, time }]);
          }
          return crowdMap;
        }, new Map<string, Omit<Crowd, 'zone'>[]>())
      )
    );
  }

  ngOnInit() {
    this.crowdData = this._crowdService.getCrowd();
    this.mapDataFromAPI();
    this.addDataSets();
  }
}
