import {Component, OnInit, ViewChild} from '@angular/core';
import { VendorService } from '../services/vendor.service';
import { Observable } from 'rxjs';
import {BaseChartDirective, Color} from 'ng2-charts';
import {ChartOptions, ChartType} from 'chart.js';
import {Device} from "../model/device";
import { Vendor } from "../model/vendor";
import { DeviceService } from "../services/device.service";

@Component({
  selector: 'cloud-frontend-piechart',
  templateUrl: './piechart.component.html',
  styleUrls: ['./piechart.component.scss']
})
export class PiechartComponent implements OnInit {

  // ADD CHART OPTIONS.
  pieChartOptions: ChartOptions = {
    responsive: true
  };

  public pieChartLabels: String[] = [];

  // CHART COLOR.
  pieChartColor: Color[] = [
    {
      backgroundColor: ['rgba(30, 169, 224, 0.8)',
        'rgba(255,165,0,0.9)',
        'rgba(139, 136, 136, 0.9)',
        'rgba(255, 161, 181, 0.9)',
        'rgba(255, 102, 0, 0.9)',
        'rgba(168,73,131,0.22)',
        'rgba(47,129,131,0.22)',
        'rgba(255,129,131,0.22)'
      ]
    }
  ];

  public pieChartData: number[] /* SingleOrMultiDataSet */ = [];
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public dataSource: Observable<Device[]>;
  public mapTrack: Map<string, string[]> = new Map<string, string[]>();

  @ViewChild(BaseChartDirective, { static: true }) chart: BaseChartDirective;

  constructor(private _deviceService: DeviceService) {}

  private updateInnerMap(d: Device) {
    if(this.mapTrack.get(d.vendor) === [] || this.mapTrack.get(d.vendor) === undefined) {
      this.mapTrack.set(d.vendor, [d['hash-mac']]);
    } else {
      if(!this.mapTrack.get(d.vendor).includes(d['hash-mac'])) {
        const newArray: string[] = this.mapTrack.get(d.vendor);
        newArray.push(d['hash-mac']);
        this.mapTrack.set(d.vendor, newArray);
      }
    }
  }

  private addDataToChart(d: Device) {
    if (!this.pieChartLabels.includes(d.vendor)) {
      this.pieChartLabels.push(d.vendor);
      this.pieChartData.push(1);
    } else {
      this.pieChartData[this.pieChartLabels.indexOf(d.vendor)] += 1;
    }
  }

  public updateObservable(firstTime: boolean) {
    this.dataSource.subscribe((data: Device[]) => {
      data.forEach((d: Device) => {
        if(firstTime) {
          this.updateInnerMap(d);
          this.addDataToChart(d);
        } else {
          if(!this.mapTrack.get(d.vendor).includes(d['hash-mac'])) {
            this.addDataToChart(d);
            this.updateInnerMap(d);
          }
        }
        this.chart.update();
      });
    });
  }

  ngOnInit() {
    this.dataSource = this._deviceService.getDevices();
    this.updateObservable(true);
  }
}
