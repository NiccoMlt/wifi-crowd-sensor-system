import { Component } from '@angular/core';

@Component({
  selector: 'cloud-frontend-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'WiFi Crowd Sensor System';
}
