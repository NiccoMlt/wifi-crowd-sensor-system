module.exports = {
  name: 'cloud-frontend',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/apps/cloud-frontend',
  snapshotSerializers: [
    'jest-preset-angular/AngularSnapshotSerializer.js',
    'jest-preset-angular/HTMLCommentSerializer.js'
  ]
};
