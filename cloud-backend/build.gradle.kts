@file:Suppress("UnstableApiUsage")

import com.moowork.gradle.node.yarn.YarnTask
import org.gradle.api.tasks.testing.logging.TestLogEvent
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
  kotlin("jvm") version Versions.org_jetbrains_kotlin_jvm_gradle_plugin
  kotlin("kapt") version Versions.org_jetbrains_kotlin_jvm_gradle_plugin
  application
  id("org.jlleitschuh.gradle.ktlint") version Versions.org_jlleitschuh_gradle_ktlint_gradle_plugin
  id("com.github.node-gradle.node") version Versions.com_github_node_gradle_node_gradle_plugin
  id("de.fayard.refreshVersions") version Versions.de_fayard_refreshversions_gradle_plugin
  jacoco
  id("com.github.johnrengelman.shadow") version Versions.com_github_johnrengelman_shadow_gradle_plugin
  id("io.vertx.vertx-plugin") version Versions.io_vertx_vertx_plugin_gradle_plugin
  // id("com.google.cloud.tools:appengine-gradle-plugin") version "2.2.0"
}

repositories {
  jcenter()
  mavenCentral()
}

dependencies {
  implementation(kotlin("stdlib-jdk8"))
  implementation(kotlin("reflect"))
  implementation(Libs.slf4j_api)
  implementation(Libs.logback_classic)
  implementation(Libs.kotlin_logging)
  implementation(Libs.vertx_core)
  implementation(Libs.vertx_lang_kotlin)
  implementation(Libs.vertx_lang_kotlin_coroutines)
  implementation(Libs.vertx_web)
  implementation(Libs.vertx_web_api_contract)
  implementation(Libs.rxjava)
  implementation(Libs.rxkotlin)
  implementation(Libs.vertx_rx_java2)
  implementation(Libs.rxfirestore)
  implementation(Libs.ipaddress)
  implementation(Libs.vertx_auth_common)
  implementation(Libs.vertx_auth_jwt)

  testImplementation(Libs.vertx_unit)
  testImplementation(Libs.vertx_junit5)
  testImplementation(kotlin("test"))
  testImplementation(kotlin("test-junit"))
  testRuntimeOnly(Libs.junit_jupiter_engine)
  testImplementation(Libs.junit_jupiter_api)
}

java {
  sourceCompatibility = Versions.java_version
  targetCompatibility = Versions.java_version
}

application {
  mainClassName = "io.vertx.core.Launcher"
}

node {
  version = Versions.node_version
  npmVersion = Versions.npm_version
  yarnVersion = Versions.yarn_version
  download = true
  nodeModulesDir = rootDir
}

vertx {
  mainVerticle = "it.unibo.crowd.MainVerticle"
  vertxVersion = Versions.io_vertx
}

tasks {
  withType<KotlinCompile> {
    kotlinOptions {
      allWarningsAsErrors = true
      jvmTarget = Versions.jdk_version
    }
  }

  val buildFrontend by creating(YarnTask::class) {
    args = listOf("build")
    inputs.files("package.json", "yarn.lock", "angular.json", "tsconfig.json")
    inputs.dir("apps/cloud-frontend")
    inputs.dir(fileTree("node_modules").exclude(".cache"))
    outputs.dir("dist/cloud-frontend")
    dependsOn("yarn")
  }

  val copyToWebRoot by creating(Copy::class) {
    from("$rootDir/dist/apps/cloud-frontend")
    destinationDir = File("$buildDir/classes/kotlin/main/webroot")
    dependsOn(buildFrontend)
  }

  "processResources"(ProcessResources::class) {
    dependsOn(copyToWebRoot)
  }

  val jest by creating(YarnTask::class) {
    setEnvironment(mapOf("CI" to "true"))
    args = listOf("test")
    dependsOn("yarn")
  }

  test {
    environment["GOOGLE_APPLICATION_CREDENTIALS"] = "keyfile.json"
    useJUnitPlatform()
    testLogging {
      events.addAll(listOf(TestLogEvent.PASSED, TestLogEvent.FAILED, TestLogEvent.SKIPPED))
    }
  }

  jacocoTestReport {
    reports {
      xml.isEnabled = false
      csv.isEnabled = false
      html.isEnabled = true
      html.destination = file("$buildDir/reports/coverage")
    }
  }

  jacocoTestCoverageVerification {
    violationRules {
      rule {
        limit {
          minimum = "0.9".toBigDecimal()
        }
      }
    }
  }

  vertxRun {
    environment["GOOGLE_APPLICATION_CREDENTIALS"] = "keyfile.json"
  }
}
