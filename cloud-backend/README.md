# Vert.x Single Page Application with Angular in NRWL NX Workspace 

## Backend

This folder is an importable Gradle project, that automatically builds also the Angular frontend and links it.

## Frontend

This folder is also an importable Angular Workspace realized with modern NRWL Nx Workspace.

The Angular application is the `cloud-frontend` folder inside [`apps` folder](./apps/cloud-frontend).
