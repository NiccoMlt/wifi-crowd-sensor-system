import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Crowd } from '../model/crowd';

@Injectable({
  providedIn: 'root'
})
export class CrowdService {

  constructor(private http: HttpClient) { }

  getCrowd(): Observable<Array<Crowd>> {
    return this.http.get<Array<Crowd>>('/api/crowd')
  }
}
