import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import { DeviceService } from '../services/device.service';
import {Observable, Subject} from "rxjs";
import {Device} from "../model/device";

@Component({
  selector: 'rpi-backend-esp-list',
  templateUrl: './esp-list.component.html',
  styleUrls: ['./esp-list.component.scss']
})

export class EspListComponent implements OnInit {
  displayedColumns: string[] = ['position', 'vendor', 'hashed mac'];
  dataSource = new MatTableDataSource<Device>();
  devices: Observable<Device[]>;
  public currentLength: Subject<number> = new Subject();

  constructor(private deviceService: DeviceService) {
    this.devices = deviceService.getDevices();
    this.refresh();
  }

  public refresh() {
    this.devices.subscribe((data: Device[]) => {
      this.currentLength.next(data.length);
      this.dataSource.data = data;
    });
  }
  ngOnInit() {}
}
