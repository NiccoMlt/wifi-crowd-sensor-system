import { BrowserModule } from '@angular/platform-browser';
import { ChartsModule } from 'ng2-charts';
import {MatTableModule} from '@angular/material/table';
import {MatFormFieldModule} from '@angular/material/form-field';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { EspListComponent } from './esp-list/esp-list.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { GraphComponent } from './graph/graph.component';
import { HttpClientModule } from '@angular/common/http';
import {MatButtonModule} from "@angular/material/button";

@NgModule({
  declarations: [AppComponent, EspListComponent, GraphComponent],
    imports: [
        BrowserModule,
        ChartsModule,
        HttpClientModule,
        MatFormFieldModule,
        MatTableModule,
        NoopAnimationsModule,
        RouterModule.forRoot([], {initialNavigation: 'enabled'}),
        MatButtonModule,
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
