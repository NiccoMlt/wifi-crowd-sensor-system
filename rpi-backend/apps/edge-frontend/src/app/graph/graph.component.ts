import { Component, OnInit } from '@angular/core';
import { ChartType, ChartDataSets, ChartOptions, ChartPoint } from 'chart.js';
import 'chartjs-plugin-streaming';
import { CrowdService } from '../services/crowd.service';
import {Observable} from 'rxjs';
import { Crowd } from '../model/crowd';

@Component({
  selector: 'rpi-backend-graph',
  templateUrl: './graph.component.html',
  styleUrls: ['./graph.component.scss']
})
export class GraphComponent implements OnInit {

  public lineChartLegend = true;
  public lineChartType: ChartType = 'line';
  public dataSets: ChartDataSets[] = [];

  public lineChartOptions: (ChartOptions | unknown) = {
    responsive: true,
    legend: {
      display: true
    },
    scales: {
      xAxes: [{
        type: 'time',
        realtime: {
          duration: 7500000, //TODO scale properly
          refresh: 6000,
          delay: 1000,
          onRefresh: chart => {
            this.crowdData.subscribe(data => {
              this.addDataToDataSet(data);
              chart.update({
                preservation: false
              });
            })
          }
        },
        distribution: 'series',
        scaleLabel: {
          display: true,
          labelString: "Time",
          fontColor: "red"
        }
      }],
      yAxes: [{
        scaleLabel: {
          display: true,
          labelString: "Crowd detected",
          fontColor: "green"
        }
      }]
    }
  };

  // @ViewChild(BaseChartDirective, { static: true }) chart: BaseChartDirective;

  crowdData: Observable<Crowd[]>;

  constructor(private _crowdService: CrowdService) {}

  private static sortArrayByDate(arrayToSort: Crowd[]): void {
    arrayToSort.sort((a: Crowd, b: Crowd) => {
      return new Date(a.time).getTime() - new Date(b.time).getTime();
    });
  }

  private addDataToDataSet(data: Crowd[]) {
    data.forEach(crowd => {
      const point: ChartPoint = {
        x: crowd.time,
        y: crowd.crowd
      };

      GraphComponent.sortArrayByDate(data);
      if(!this.dataSets[0].data.some((x: number & ChartPoint) => x.x === point.x && x.y === point.y)){
        if(GraphComponent.cutOldData(point.x as string)) {
          this.dataSets[0].data.push(point as number & ChartPoint)
        }
      }
    });
  }

  private static cutOldData(pointDate: string): boolean {
    let now = new Date();
    now.setHours(now.getHours() - 2);
    return new Date(now).getTime() < new Date(pointDate).getTime();
  }

  private addDataSets() {
    this.crowdData.subscribe((data: Crowd[]) => {
      const labels: ChartDataSets[] = [];
      labels.push({
        label: data[0].zone,
        data: []
      });
      this.dataSets.push(...labels);
      this.addDataToDataSet(data);
    });
  }

  ngOnInit() {
    this.crowdData = this._crowdService.getCrowd();
    this.addDataSets();
  }
}
