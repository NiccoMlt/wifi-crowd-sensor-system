module.exports = {
  name: 'edge-frontend',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/apps/edge-frontend',
  snapshotSerializers: [
    'jest-preset-angular/AngularSnapshotSerializer.js',
    'jest-preset-angular/HTMLCommentSerializer.js'
  ]
};
