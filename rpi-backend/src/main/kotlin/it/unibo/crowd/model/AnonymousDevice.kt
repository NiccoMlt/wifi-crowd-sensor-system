package it.unibo.crowd.model

import com.fasterxml.jackson.annotation.JsonProperty

/**
 * The interface models an anonymized version of [Device].
 *
 * @param hashedMac the MAC address anonymized with an hash function
 * @param vendor the vendor obtained from MAC address before anonymization
 */
data class AnonymousDevice(
  @field:JsonProperty(value = "hash-mac")
  val hashedMac: String,
  @field:JsonProperty(value = "vendor")
  val vendor: String
)
