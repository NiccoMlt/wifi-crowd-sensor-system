package it.unibo.crowd

import io.vertx.core.logging.SLF4JLogDelegateFactory
import io.vertx.kotlin.core.deployVerticleAwait
import io.vertx.kotlin.coroutines.CoroutineVerticle
import it.unibo.crowd.backend.BackendVerticle
import it.unibo.crowd.backend.mqtt.MqttVerticle

/**
 * Entrypoint Verticle, launched by [Vert.x Launcher][io.vertx.core.Launcher];
 * it deploys all other verticles to start the application up.
 *
 * It also automatically sets logger delegate for [SLF4J][io.vertx.core.logging.SLF4JLogDelegate].
 */
class MainVerticle : CoroutineVerticle() {
  companion object {
    private const val LOGGER_DELEGATE_FACTORY_PROPERTY = "vertx.logger-delegate-factory-class-name"
  }

  override suspend fun start() {
    if (System.getProperty(LOGGER_DELEGATE_FACTORY_PROPERTY) == null) {
      System.setProperty(
        LOGGER_DELEGATE_FACTORY_PROPERTY,
        SLF4JLogDelegateFactory::class.qualifiedName ?: throw ClassNotFoundException()
      )
    }

    vertx.deployVerticleAwait(BackendVerticle())
    vertx.deployVerticleAwait(MqttVerticle())
  }
}
