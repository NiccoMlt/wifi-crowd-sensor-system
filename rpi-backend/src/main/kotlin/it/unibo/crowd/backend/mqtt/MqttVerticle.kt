package it.unibo.crowd.backend.mqtt

import io.vertx.config.ConfigRetriever
import io.vertx.core.CompositeFuture
import io.vertx.core.Context
import io.vertx.core.Promise
import io.vertx.core.Vertx
import io.vertx.core.buffer.Buffer
import io.vertx.core.json.Json
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.client.HttpResponse
import io.vertx.ext.web.client.WebClient
import io.vertx.kotlin.core.json.get
import io.vertx.kotlin.coroutines.CoroutineVerticle
import io.vertx.kotlin.mqtt.connectAwait
import io.vertx.mqtt.MqttClient
import io.vertx.mqtt.messages.MqttPublishMessage
import it.unibo.crowd.backend.routes.resolveEdgeConf
import it.unibo.crowd.backend.routes.resolveLocationConf
import it.unibo.crowd.model.AnonymousDevice
import it.unibo.crowd.model.Device
import java.time.LocalDateTime
import mu.KotlinLogging

class MqttVerticle : CoroutineVerticle() {
  private val logger = KotlinLogging.logger {}
  private lateinit var mqttClient: MqttClient
  private lateinit var httpClient: WebClient
  private lateinit var configRetriever: ConfigRetriever

  override fun init(vertx: Vertx, context: Context) {
    super.init(vertx, context)
    mqttClient = MqttClient.create(vertx)
    httpClient = WebClient.create(vertx)
    configRetriever = ConfigRetriever.create(vertx)
  }

  override suspend fun start() {
    val (api, mqtt) = resolveEdgeConf(configRetriever)
    val (apiHost, apiPort) = api
    val (mqttHost, mqttPort, topic) = mqtt
    val location = resolveLocationConf(configRetriever)
    mqttClient.connectAwait(mqttPort, mqttHost)

    mqttClient
      .publishHandler { s: MqttPublishMessage ->
        val date = LocalDateTime.now()
        val jsonArray: JsonArray = s.payload().toJsonObject()["MAC"]
        println("MQTT PUBLISH: $jsonArray")
        val nDevices: Int = jsonArray.size()
        val devices: List<AnonymousDevice> = jsonArray.map { Device(it as String).anonymize() }
        CompositeFuture
          .join(devices.map {
            val promise = Promise.promise<HttpResponse<Buffer>>()
            httpClient
              .put(apiPort, apiHost, "/api/devices")
              .sendJson(JsonObject(Json.encode(it)), promise)
            return@map promise.future()
          })
          .setHandler { api ->
            if (api.succeeded()) {
              val crowd = JsonObject()
              crowd.put("time", date.toString())
              crowd.put("zone", location)
              crowd.put("crowd", nDevices)
              httpClient
                .put(apiPort, apiHost, "/api/crowd")
                .sendJson(crowd) { count ->
                  if (count.succeeded()) {
                    logger.info { "RESPONSE WAS ${count.result().statusMessage()} ${count.result().bodyAsString()}" }
                    logger.info { "Successfully handled MQTT publish: $crowd" }
                  } else {
                    logger.error(count.cause()) { "Self API call for crowd counter failed on MQTT publish" }
                  }
                }
            } else {
              logger.error(api.cause()) { "Self API call for device MACs failed on MQTT publish" }
            }
          }
      }
      .subscribe(topic, 2) {
        if (it.succeeded()) {
          logger.info { "Subscribed successfully with code ${it.result()}" }
        } else {
          logger.error { "MQTT subscription failed: ${it.cause().localizedMessage}" }
        }
      }
  }
}
