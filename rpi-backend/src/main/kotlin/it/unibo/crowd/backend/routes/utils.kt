package it.unibo.crowd.backend.routes

import io.vertx.config.ConfigRetriever
import io.vertx.core.AsyncResult
import io.vertx.core.Handler
import io.vertx.core.buffer.Buffer
import io.vertx.core.http.HttpMethod
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.RoutingContext
import io.vertx.ext.web.client.HttpResponse
import io.vertx.ext.web.client.WebClient
import io.vertx.kotlin.config.getConfigAwait
import io.vertx.kotlin.core.json.get
import io.vertx.kotlin.core.json.jsonObjectOf
import io.vertx.kotlin.ext.web.client.sendJsonAwait
import mu.KotlinLogging

/**
 * Resolve host and port pair from config.
 *
 * @param configRetriever the entry point to config.
 * @return the host and port pair
 */
suspend fun resolveCloudConf(configRetriever: ConfigRetriever): Pair<String, Int> {
  val conf: JsonObject = configRetriever.getConfigAwait()
  val cloud: JsonObject = conf["cloud"]
  val port: Int = cloud["port"]
  val host: String = cloud["host"]
  return Pair(host, port)
}

suspend fun resolveLocationConf(configRetriever: ConfigRetriever): String = configRetriever.getConfigAwait()["location"]

/**
 * Resolve host, port and location triple from config.
 *
 * @param configRetriever the entry point to config.
 * @return the host, port and location triple
 */
suspend fun resolveCloudConfWithLocation(configRetriever: ConfigRetriever): Triple<String, Int, String> {
  val conf: JsonObject = configRetriever.getConfigAwait()
  val location: String = conf["location"]
  val cloud: JsonObject = conf["cloud"]
  val port: Int = cloud["port"]
  val host: String = cloud["host"]

  return Triple(host, port, location)
}

suspend fun resolveEdgeConf(configRetriever: ConfigRetriever): Pair<Pair<String, Int>, Triple<String, Int, String>> {
  val conf: JsonObject = configRetriever.getConfigAwait()
  val edge: JsonObject = conf["edge"]

  val api: JsonObject = edge["api"]
  val apiPort: Int = api["port"]
  val apiHost: String = api["host"]

  val mqtt: JsonObject = edge["mqtt"]
  val mqttPort: Int = mqtt["port"]
  val mqttHost: String = mqtt["host"]
  val topic: String = mqtt["topic"]

  println("EDGE CONF is $mqtt")

  return Pair(
    Pair(apiHost, apiPort),
    Triple(mqttHost, mqttPort, topic)
  )
}

/**
 * Do a remote HTTP call to cloud.
 *
 * @param client the client used to contact the cloud
 * @param host the cloud host
 * @param port the cloud port
 * @param route the cloud route
 * @param method the HTTP method to use
 *
 * @return the handler (a function) for Vert.X
 */
fun connectTo(
  client: WebClient,
  host: String,
  port: Int,
  route: String,
  method: HttpMethod,
  useBody: Boolean = false,
  bearer: String? = null
) =
  Handler { routingContext: RoutingContext ->
    val req = client.request(method, port, host, route)

    if (bearer != null) req.bearerTokenAuthentication(bearer)

    val callback: Handler<AsyncResult<HttpResponse<Buffer>>> = Handler {
      if (it.succeeded()) {
        val result = it.result()
        val resp = routingContext
          .response()
          .setStatusCode(result.statusCode())
          .setStatusMessage(result.statusMessage())
        val otherHandler: Handler<AsyncResult<Void>> = Handler { otherRes ->
          if (otherRes.succeeded()) {
            println("Tutto OK")
          } else {
            println("Errore: ${otherRes.cause()}")
          }
        }
        if (result.body() != null) {
          resp.end(result.body(), otherHandler)
        } else {
          resp.end(otherHandler)
        }
      } else {
        routingContext.response().setStatusCode(500).end(it.cause().localizedMessage)
      }
    }
    if (useBody) {
      req.sendJson(routingContext.bodyAsJson, callback)
    } else {
      req.send(callback)
    }
  }

suspend fun requestToken(client: WebClient, configRetriever: ConfigRetriever): String {
  val (host, port) = resolveCloudConf(configRetriever)
  val location = resolveLocationConf(configRetriever)
  val key: String = configRetriever.getConfigAwait()["key"]
  val json = jsonObjectOf(
    "location" to location,
    "key" to key
  )
  KotlinLogging.logger {}.info { "Connecting to $host at port $port sending $json" }
  val result = client
    .post(port, host, "/api/auth")
    .sendJsonAwait(json)
  return result.bodyAsJsonObject()["token"]
}
