package it.unibo.crowd.backend

import io.vertx.config.ConfigRetriever
import io.vertx.core.Context
import io.vertx.core.Vertx
import io.vertx.ext.web.Router
import io.vertx.ext.web.client.WebClient
import io.vertx.ext.web.handler.LoggerFormat
import io.vertx.ext.web.handler.LoggerHandler
import io.vertx.ext.web.handler.StaticHandler
import io.vertx.kotlin.config.getConfigAwait
import io.vertx.kotlin.core.http.listenAwait
import io.vertx.kotlin.core.json.get
import io.vertx.kotlin.coroutines.CoroutineVerticle
import io.vertx.kotlin.ext.web.api.contract.openapi3.OpenAPI3RouterFactory
import it.unibo.crowd.backend.routes.addDevice
import it.unibo.crowd.backend.routes.addMeasure
import it.unibo.crowd.backend.routes.getCrowd
import it.unibo.crowd.backend.routes.getDevices
import it.unibo.crowd.backend.routes.getLatestCrowd
import it.unibo.crowd.backend.routes.requestToken
import it.unibo.crowd.backend.routes.resolveEdgeConf
import mu.KotlinLogging

/**
 * This verticle serves the React.JS application and implement OpenAPI contract for REST APIs.
 */
class BackendVerticle : CoroutineVerticle() {
  private lateinit var client: WebClient

  override fun init(vertx: Vertx, context: Context) {
    super.init(vertx, context)
    client = WebClient.create(vertx)
  }

  override suspend fun start() {
    val conf: ConfigRetriever = ConfigRetriever.create(vertx)
    val (apiConf, _) = resolveEdgeConf(conf)
    val (_, port) = apiConf
    val token: String = try {
      val tk = requestToken(client, conf)
      KotlinLogging.logger {}.info { "Token obtained: $tk" }
      tk
    } catch (ex: Exception) {
      val dummyToken: String = conf.getConfigAwait()["dummy-token"]
      KotlinLogging.logger {}.error(ex) { "Can't fetch token, using dummy: $dummyToken" }
      dummyToken
    }

    val router: Router = Router.router(vertx)
    router.route().handler(LoggerHandler.create(LoggerFormat.SHORT))
    val apiRouter: Router = OpenAPI3RouterFactory
      .createAwait(vertx, "/openapi.yaml")
      .addHandlerByOperationId("getCrowd", getCrowd(client, conf))
      .addHandlerByOperationId("addMeasure", addMeasure(client, conf, token))
      .addHandlerByOperationId("getLatestCrowd", getLatestCrowd(client, conf))
      .addHandlerByOperationId("getDevices", getDevices(client, conf))
      .addHandlerByOperationId("addDevice", addDevice(client, conf, token))
      .router
    router.mountSubRouter("/api/", apiRouter)
    router.get().handler(StaticHandler.create())

    vertx
      .createHttpServer()
      .requestHandler(router)
      .listenAwait(port)

    KotlinLogging.logger {}.info { "HTTP server started on port $port" }
  }
}
