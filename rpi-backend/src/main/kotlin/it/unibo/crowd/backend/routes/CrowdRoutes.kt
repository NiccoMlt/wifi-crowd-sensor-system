@file:JvmName("CrowdRoutes")

package it.unibo.crowd.backend.routes

import io.vertx.config.ConfigRetriever
import io.vertx.core.Handler
import io.vertx.core.http.HttpMethod
import io.vertx.ext.web.RoutingContext
import io.vertx.ext.web.client.WebClient

/**
 * Generate a route handler for getCrowd operationId.
 *
 * @param client the client used to contact the cloud.
 * @param configRetriever the entry point to config.
 *
 * @return the handler (a function) for Vert.X
 */
suspend fun getCrowd(client: WebClient, configRetriever: ConfigRetriever): Handler<RoutingContext> {
  val (host, port) = resolveCloudConf(configRetriever)
  return connectTo(client, host, port, "/api/crowd", HttpMethod.GET)
}

/**
 * Generate a route handler for addMeasure operationId.
 *
 * @param client the client used to contact the cloud.
 * @param configRetriever the entry point to config.
 *
 * @return the handler (a function) for Vert.X
 */
suspend fun addMeasure(client: WebClient, configRetriever: ConfigRetriever, token: String): Handler<RoutingContext> {
  val (host, port, location) = resolveCloudConfWithLocation(configRetriever)
  return connectTo(client, host, port, "/api/crowd/$location", HttpMethod.PUT, useBody = true, bearer = token)
}

/**
 * Generate a route handler for getLatestCrowd operationId.
 *
 * @param client the client used to contact the cloud.
 * @param configRetriever the entry point to config.
 *
 * @return the handler (a function) for Vert.X
 */
suspend fun getLatestCrowd(client: WebClient, configRetriever: ConfigRetriever): Handler<RoutingContext> {
  val (host, port, location) = resolveCloudConfWithLocation(configRetriever)
  return connectTo(client, host, port, "/api/crowd/$location/latest", HttpMethod.GET)
}
