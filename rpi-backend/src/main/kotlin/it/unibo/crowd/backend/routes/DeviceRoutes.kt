@file:JvmName("DeviceRoutes")

package it.unibo.crowd.backend.routes

import io.vertx.config.ConfigRetriever
import io.vertx.core.Handler
import io.vertx.core.http.HttpMethod
import io.vertx.ext.web.RoutingContext
import io.vertx.ext.web.client.WebClient

suspend fun getDevices(client: WebClient, configRetriever: ConfigRetriever): Handler<RoutingContext> {
  val (host, port) = resolveCloudConf(configRetriever)
  return connectTo(client, host, port, "/api/devices", HttpMethod.GET)
}

suspend fun addDevice(client: WebClient, configRetriever: ConfigRetriever, token: String): Handler<RoutingContext> {
  val (host, port, location) = resolveCloudConfWithLocation(configRetriever)
  println("host $host at port $port for location $location")
  return connectTo(client, host, port, "/api/devices/$location", HttpMethod.PUT, useBody = true, bearer = token)
}
